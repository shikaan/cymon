import * as angular from 'angular';
import 'normalize.css';
import 'flexboxgrid/dist/flexboxgrid.min.css';
import './assets/fonts/fonts.scss';
import './assets/style/reset.scss';

import scores from './app/scores/scores.component';
import board from './app/board/board.component';
import simon from './app/app.component';
import footer from './app/footer-bar/footer-bar.component';

angular.module('simon', [])
    .component('simon', simon)
    .component('scores', scores)
    .component('board', board)
    .component('footerBar', footer);