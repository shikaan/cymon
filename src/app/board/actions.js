const ACTION_TYPE = {
    PROMPT_VALUE: 'PROMPT_VALUE',
    PLAY_SEQUENCE: 'PLAY_SEQUENCE'
}

function promptValue(value){
    return {
        type: ACTION_TYPE.PROMPT_VALUE,
        value
    }
}

function playSequence(){
    return {type: ACTION_TYPE.PLAY_SEQUENCE}
}

export {promptValue, playSequence, ACTION_TYPE}